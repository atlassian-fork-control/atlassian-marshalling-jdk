package com.atlassian.marshalling.jdk;

import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class StringMarshallingTest {
    @Test(expected = MarshallingException.class)
    public void testIllegalString()  {
        final Marshaller<String> sm = new StringMarshalling();
        final String instr = "Illegal string because of unicode character -> \uD800";
        final byte[] raw = sm.marshallToBytes(instr);
    }

    @Test
    public void testLegalString()  {
        final MarshallingPair<String> mp = StringMarshalling.pair();
        final String instr = "Josie likes Football";
        final byte[] raw = mp.getMarshaller().marshallToBytes(instr);

        assertThat(raw, notNullValue());

        final String outstr = mp.getUnmarshaller().unmarshallFrom(raw);

        assertThat(outstr, notNullValue());
        assertThat(outstr, is(instr));
    }
}
