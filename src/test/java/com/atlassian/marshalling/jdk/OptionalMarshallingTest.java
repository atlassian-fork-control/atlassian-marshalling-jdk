package com.atlassian.marshalling.jdk;

import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

public class OptionalMarshallingTest {
    private MarshallingPair<Optional<String>> marshallingPair = OptionalMarshalling.pair(StringMarshalling.pair());

    @Test
    public void emptyValue() throws MarshallingException {
        final byte[] raw = marshallingPair.getMarshaller().marshallToBytes(Optional.empty());

        assertArrayEquals(new byte[] {0}, raw);

        final Optional<String> revived = marshallingPair.getUnmarshaller().unmarshallFrom(raw);

        assertThat(revived, notNullValue());
        assertThat(revived, is(Optional.empty()));
    }

    @Test
    public void withValue() throws MarshallingException {
        final byte[] raw = marshallingPair.getMarshaller().marshallToBytes(Optional.of("claira"));

        assertArrayEquals(new byte[] {1, 99, 108, 97, 105, 114, 97}, raw);

        final Optional<String> revived = marshallingPair.getUnmarshaller().unmarshallFrom(raw);

        assertThat(revived, notNullValue());
        assertThat(revived, is(Optional.of("claira")));
    }

    @Test
    public void withValue_empty() throws MarshallingException {
        final byte[] raw = marshallingPair.getMarshaller().marshallToBytes(Optional.of(""));

        assertArrayEquals(new byte[] {1}, raw);

        final Optional<String> revived = marshallingPair.getUnmarshaller().unmarshallFrom(raw);

        assertThat(revived, notNullValue());
        assertThat(revived, is(Optional.of("")));
    }
}
