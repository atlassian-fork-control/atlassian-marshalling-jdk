package com.atlassian.marshalling.jdk;

import com.atlassian.marshalling.api.MarshallingPair;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ByteMarshallingTest {
    @Test
    public void ByteRoundTrip() throws Exception {
        final MarshallingPair<byte[]> smp = ByteMarshalling.pair();

        final byte[] data = smp.getMarshaller().marshallToBytes("Art Vs Science".getBytes());
        final byte[] revived = smp.getUnmarshaller().unmarshallFrom(data);

        assertThat(new String(revived, "UTF-8"), is("Art Vs Science"));
    }

}