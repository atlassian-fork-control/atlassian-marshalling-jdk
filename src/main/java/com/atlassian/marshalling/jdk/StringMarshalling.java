package com.atlassian.marshalling.jdk;

import com.atlassian.annotations.PublicApi;
import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.api.Unmarshaller;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Optimised implementation for {@link String} objects.
 *
 * @since 1.0.0
 */
@PublicApi
public class StringMarshalling implements Marshaller<String>, Unmarshaller<String> {
    private static final Charset ENCODING_CHARSET = StandardCharsets.UTF_8;

    @Override
    public byte[] marshallToBytes(String str) throws MarshallingException {
        // Need to have the encoder complain if it cannot encode a character. By default it swallows the problem.
        final CharsetEncoder encoder = ENCODING_CHARSET.newEncoder();
        encoder.onMalformedInput(CodingErrorAction.REPORT);

        try {
            final ByteBuffer buffer = encoder.encode(CharBuffer.wrap(str));
            return Arrays.copyOf(buffer.array(), buffer.limit());
        } catch (CharacterCodingException e) {
            throw new MarshallingException("Unable to encode: " + str, e);
        }
    }

    @Override
    public String unmarshallFrom(byte[] raw) throws MarshallingException {
        return new java.lang.String(raw, ENCODING_CHARSET);
    }

    /**
     * @return a {@link MarshallingPair}.
     */
    public static MarshallingPair<String> pair() {
        final StringMarshalling sm = new StringMarshalling();
        return new MarshallingPair<>(sm, sm);
    }
}
