package com.atlassian.marshalling.jdk;

import com.atlassian.annotations.PublicApi;
import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.api.Unmarshaller;

import java.util.Arrays;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Implementation for the {@link Optional} class.
 *
 * @param <T> the type held in the Optional.
 * @since 1.0.0
 */
@PublicApi
public class OptionalMarshalling<T> implements Marshaller<Optional<T>>, Unmarshaller<Optional<T>> {
    // Implementation note: unable to use an empty byte array to signify that there is no value. This is because
    // it is legitimate for a Marshaller to return a zero length byte array. E.g. the StringMarshaller does this.
    // Hence this implementation uses the first byte of the array to indicate whether there is a value (or not).
    // The downside is that Marshaller interface forces us to do array copying, which should hopefully be fast.

    private final MarshallingPair<T> valueMarshallingPair;

    /**
     * Creates an instance.
     *
     * @param valueMarshallingPair the marshalling pair to use for the actual value.
     */
    public OptionalMarshalling(MarshallingPair<T> valueMarshallingPair) {
        this.valueMarshallingPair = requireNonNull(valueMarshallingPair);
    }

    @Override
    public byte[] marshallToBytes(Optional<T> obj) throws MarshallingException {
        if (!obj.isPresent()) {
            return new byte[]{0};
        }

        final byte[] valueBytes = valueMarshallingPair.getMarshaller().marshallToBytes(obj.get());
        final byte[] resultBytes = new byte[valueBytes.length + 1];
        resultBytes[0] = 1;
        System.arraycopy(valueBytes, 0, resultBytes, 1, valueBytes.length);
        return resultBytes;
    }

    @Override
    public Optional<T> unmarshallFrom(byte[] raw) throws MarshallingException {
        if (raw[0] == 0) {
            return Optional.empty();
        }

        final byte[] valueBytes = Arrays.copyOfRange(raw, 1, raw.length);

        return Optional.of(valueMarshallingPair.getUnmarshaller().unmarshallFrom(valueBytes));
    }

    /**
     * @param valueMarshallingPair the marshalling pair to use for the actual value.
     * @param <T>                  the type held in the Optional.
     * @return a {@link MarshallingPair}.
     */
    public static <T> MarshallingPair<Optional<T>> pair(MarshallingPair<T> valueMarshallingPair) {
        final OptionalMarshalling<T> sm = new OptionalMarshalling<>(valueMarshallingPair);
        return new MarshallingPair<>(sm, sm);
    }
}
